import React, { Component } from 'react';
import Chart, { ChartConfiguration } from 'chart.js';

interface GraphProps {
  title: string;
  subtitle: string;
  data: ChartConfiguration;
}

export default class Graph extends Component<GraphProps> {
  myChart: any = {};
  chartRef = React.createRef<HTMLCanvasElement>();

  componentDidMount() {
    const { type, data, options } = this.props.data;

    this.myChart = new Chart(this.chartRef.current!, {
      type,
      data,
      options
    });
  }

  render() {
    return (
      <div>
        <h3>{this.props.title}</h3>
        <p>{this.props.subtitle}</p>
        <canvas ref={this.chartRef} />
      </div>
    );
  }
}
