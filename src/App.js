import React from 'react';
import Graph from './Graph';

import './App.css';

import {
  getBarDataParking,
  getBarDataParkingSession,
  getPieDataParkingTime,
  getLineDataDTP
} from './chart-data';

class App extends React.Component {
  state = {
    barDataParking: getBarDataParking(),
    barDataSession: getBarDataParkingSession(),
    pieDataParkingTime: getPieDataParkingTime(),
    lineDataDTP: getLineDataDTP()
  };
  render() {
    const {
      barDataParking,
      barDataSession,
      pieDataParkingTime,
      lineDataDTP
    } = this.state;

    return (
      <div className='container'>
        <div className='container-chart'>
          <Graph
            data={lineDataDTP}
            title='Количество раненых по видам ДТП'
            subtitle='Количество'
          />
        </div>
        <div className='container-chart'>
          <Graph
            data={barDataParking}
            title='Собираемость парковок'
            subtitle='Тыс. руб'
          />
        </div>
        <div className='container-chart'>
          <Graph
            data={barDataSession}
            title='Количество парковочных сессий'
            subtitle='Количество'
          />
        </div>
        <div className='container-chart'>
          <Graph
            data={pieDataParkingTime}
            title='Общая длительность парковочных сессий'
            subtitle='Часы'
          />
        </div>
      </div>
    );
  }
}

export default App;
