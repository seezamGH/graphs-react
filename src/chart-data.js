// 1. Установить цвета из макета

export function getBarDataParking() {
  return {
    type: 'bar',
    data: {
      labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
      datasets: [
        {
          label: 'Test Chart',
          data: getRandom(0, 4000, 6),
          backgroundColor: '#FCAF62',
          borderColor: '#FCAF62',
          borderWidth: 1
        }
      ]
    },
    options: {
      legend: {
        display: false
      },
      scales: {
        yAxes: [
          {
            ticks: {
              beginAtZero: true,
              min: 0,
              max: 4000,
              stepSize: 1000
            }
          }
        ],
        xAxes: [
          {
            display: false
          }
        ]
      }
    }
  };
}

export function getBarDataParkingSession() {
  return {
    type: 'bar',
    data: {
      labels: [
        'Red',
        'Blue',
        'Yellow',
        'Green',
        'Purple',
        'Orange',
        'seven',
        'eight'
      ],
      datasets: [
        {
          label: 'Test Chart',
          data: getRandom(0, 8000, 9),
          backgroundColor: '#F48558',
          borderColor: '#F48558',
          borderWidth: 1
        }
      ]
    },
    options: {
      legend: {
        display: false
      },
      scales: {
        yAxes: [
          {
            ticks: {
              beginAtZero: true,
              min: 0,
              max: 8000,
              stepSize: 2000
            }
          }
        ],
        xAxes: [
          {
            display: false
          }
        ]
      }
    }
  };
}

// 1. Установить цвета из макета
// 3. Сделать labels круглыми как в макете

// 1. Установить цвета из макета
// 3. Сделать labels круглыми как в макете

export function getPieDataParkingTime() {
  return {
    type: 'pie',
    data: {
      labels: ['1-2', '2-3', '3-4', '5-6', '7-8', '9-10', '11-12'],
      datasets: [
        {
          label: 'Test Chart',
          data: getRandom(0, 20, 7),
          backgroundColor: [
            '#E1551C',
            '#F8B632',
            '#25BE5E',
            '#5597F3',
            '#29BAF8',
            '#3E13BF',
            '#E68423'
          ],
          borderColor: [
            '#E1551C',
            '#F8B632',
            '#25BE5E',
            '#5597F3',
            '#29BAF8',
            '#3E13BF',
            '#E68423'
          ],
          borderWidth: 1
        }
      ]
    },
    options: {
      legend: {
        position: 'left',
        display: true,
        labels: {
          fontColor: '#797C80'
        }
        // usePointStyle: true
      },
      scales: {
        yAxes: [
          {
            ticks: {
              beginAtZero: true
            },
            display: false
          }
        ]
      }
    }
  };
}

// 1. Установить цвета из макета

export function getLineDataDTP() {
  return {
    type: 'line',
    data: {
      labels: ['12. 02', '15. 02', '18. 02', '20. 02', '25. 02', '28. 02'],
      datasets: [
        {
          label: 'Количество раненых по видам ДТП',
          data: getRandom(0, 400, 6), // данные с сервера
          borderColor: '#E1551C',
          borderWidth: 2,
          pointRadius: 0,
          fill: false
        },
        {
          label: 'Количество раненых по видам ДТП',
          data: [225, 222, 128, 393, 335, 210], // данные с сервера
          borderColor: '#5597F3',
          borderWidth: 2,
          pointRadius: 0,
          fill: false
        },
        {
          label: 'Количество раненых по видам ДТП',
          data: [320, 215, 313, 115, 10, 125], // данные с сервера
          borderColor: '#F8B632',
          borderWidth: 2,
          pointRadius: 0,
          fill: false
        }
      ]
    },
    options: {
      legend: {
        display: false
      },
      scales: {
        yAxes: [
          {
            ticks: {
              beginAtZero: true,
              min: 0,
              max: 400,
              stepSize: 100
            }
          }
        ],
        xAxes: [
          {
            display: false
          }
        ]
      }
    }
  };
}

function getRandom(min, max, maxLength) {
  const arr = [];

  for (let i = 0; i < maxLength; i++) {
    arr.push(Math.floor(Math.random() * (max - min)) + min);
  }

  return arr;
}
